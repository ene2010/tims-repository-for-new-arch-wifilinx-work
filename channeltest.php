<!DOCTYPE HTML>
<html>
<head>
<title>Roomlinx Backbone Test</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<!--
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.cdnjs.com/ajax/libs/underscore.js/1.1.4/underscore-min.js"></script>
<script type="text/javascript" src="http://ajax.cdnjs.com/ajax/libs/backbone.js/0.3.3/backbone-min.js"></script>

<script type="text/javascript" src="http://ajax.cdnjs.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
-->
<script type="text/javascript" src="lib/org/underscore/underscore-min.js"></script>
<script type="text/javascript" src="lib/org/backbone/backbone.js"></script>
<!-- ><script type="text/javascript" src="lib/rmlx/structr/structr.js"></script> -->

<style>

body {
	background: #AAA;
}

.container {
	/* width:980px; */
	/* width:1300px; */
	width:70%;
	height:600px;
	margin: 0 auto;
	padding: 20px;
	background: #888;
}

.rounded  {
	-moz-border-radius: 10px;
	-webkit-border-radius: 10px;
	border-radius: 10px;
	-moz-box-shadow: 	4px 4px 4px #555;
	-webkit-box-shadow: 0px 4px 4px #555;
	box-shadow: 	4px 4px 4px #555;
}

.rounded_tight  {
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;
}


.hide {
	display:none;
}

.skin_whitish {
	background: #EEE;
	border: 1px solid #000;	
	color: #000;
}

.skin_greyish {
	background: #222;
	border: none;
	color: #FFF;
}

.skin_greyish_blue {
	background: #223;
	border: none;
	color: #FFF;
}

.skin_clear {
	border-bottom: 1px solid #000;
}

.skin_grey_gradient {
	background: -moz-linear-gradient(top, #3C4043 0%, #3D3C3A 20%, #323232 40%, #2E2E2E 60%, #242424 80%, #1F1F1F 100%); /* FF3.6+ */
	background: -webkit-linear-gradient(top, #3C4043 0%, #3D3C3A 20%, #323232 40%, #2E2E2E 60%, #242424 80%, #1F1F1F 100%); /* Chrome10+,Safari5.1+ */
	background: -webkit-gradient(linear, left top, left bottom, 
		color-stop(0%,#3C4043), color-stop(20%,#3D3C3A), color-stop(40%,#323232),
		color-stop(60%,#2E2E2E), color-stop(80%,#242424), color-stop(100%,#1F1F1F)); /* Chrome,Safari4+ */
	background: -o-linear-gradient(top, #3C4043 0%, #3D3C3A 20%, #323232 40%, #2E2E2E 60%, #242424 80%, #1F1F1F 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #3C4043 0%, #3D3C3A 20%, #323232 40%, #2E2E2E 60%, #242424 80%, #1F1F1F 100%); /* IE10+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3C4043', endColorstr='#1F1F1F',GradientType=0 );
}

.skin_deep_blue_gradient {
    background: -moz-linear-gradient(top, #212E44 0%, #1E2A3C 20%, #1B2534 40%, #09101A 60%, #080E15 80%, #080C11 100%); /* FF3.6+ */
	background: -webkit-linear-gradient(top, #212E44 0%, #1E2A3C 20%, #1B2534 40%, #09101A 60%, #080E15 80%, #080C11 100%); /* Chrome10+,Safari5.1+ */
	background: -webkit-gradient(linear, left top, left bottom, 
		color-stop(0%,#212E44), color-stop(20%,#1E2A3C), color-stop(40%,#1B2534),
		color-stop(60%,#09101A), color-stop(80%,#080E15), color-stop(100%,#080C11)); /* Chrome,Safari4+ */
	background: -o-linear-gradient(top, #212E44 0%, #1E2A3C 20%, #1B2534 40%, #09101A 60%, #080E15 80%, #080C11 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #212E44 0%, #1E2A3C 20%, #1B2534 40%, #09101A 60%, #080E15 80%, #080C11 100%); /* IE10+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#212E44', endColorstr='#080C11',GradientType=0 );
}

.skin_light_blue_gradient {
    background: -moz-linear-gradient(top, #3165A6 0%, #2A6099 20%, #295A91 40%, #224774 60%, #1D3B63 80%, #1B3152 100%); /* FF3.6+ */
	background: -webkit-linear-gradient(top, #3165A6 0%, #2A6099 20%, #295A91 40%, #224774 60%, #1D3B63 80%, #1B3152 100%); /* Chrome10+,Safari5.1+ */
	background: -webkit-gradient(linear, left top, left bottom, 
		color-stop(0%,#3165A6), color-stop(20%,#2A6099), color-stop(40%,#295A91),
		color-stop(60%,#224774), color-stop(80%,#1D3B63), color-stop(100%,#1B3152)); /* Chrome,Safari4+ */
	background: -o-linear-gradient(top, #3165A6 0%, #2A6099 20%, #295A91 40%, #224774 60%, #1D3B63 80%, #1B3152 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #3165A6 0%, #2A6099 20%, #295A91 40%, #224774 60%, #1D3B63 80%, #1B3152 100%); /* IE10+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3165A6', endColorstr='#1B3152',GradientType=0 );
}


.skin_grey_button {
	position:absolute;
    margin: 0 auto;  
	display:block;
	z-index:2;
	
	border: 1px solid #111;
	
	-moz-border-radius: 10px;
	-webkit-border-radius: 10px;
	border-radius: 10px;
	-moz-box-shadow: 	1px 1px 1px #888;
	-webkit-box-shadow: 0px 1px 1px #888;
	box-shadow: 	1px 1px 1px #888;	
	
	background-color: #999;
	background: -moz-linear-gradient(top, #2B2C2D 0%, #3B3C3E 5%, #424242 25%, #2A2B2D 100%); 
	/* background: -moz-linear-gradient(top, #2B2C2D 0%, #3B3C3E 5%, #424242 25%, #373737 50%, #2F3032 75%, #212123 95%, #2A2B2D 100%); */
	

	background: -webkit-gradient(linear, left top, left bottom, 
		color-stop(0%,#2B2C2D), color-stop(5%,#3B3C3E), color-stop(25%,#424242),color-stop(100%,#2A2B2D)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #2B2C2D 0%, #3B3C3E 5%, #424242 25%, #2A2B2D 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #2B2C2D 0%, #3B3C3E 5%, #424242 25%, #2A2B2D 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #2B2C2D 0%, #3B3C3E 5%, #424242 25%, #2A2B2D 100%); /* IE10+ */
	/* filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='##2B2C2D', endColorstr='##2A2B2D',GradientType=0 ) */
	/* filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3C4043', endColorstr='#1F1F1F',GradientType=0 ); */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4C5053', endColorstr='#2F2F2F',GradientType=0 );
}	


.skin_grey_button:before {
    content: "";  
    width: 75px;  
    height: 20px;  
	opacity:0.5;
  
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;  
  
  
    display: block;  
    position: absolute;  
	top:3px;
    left: 1px;  
	background: -moz-linear-gradient(left, #828385 0%, #828385 2%, #828385 3%, #828385 5%, #373737 10%,   #828385 31%, 
									#828385 75%, #373737 90%, #828385 95%,#828385 97%, #828385 98%,#828385 100%); 
	/* background: -moz-linear-gradient(left, #828385 0%, #828385 2%, #828385 3%, #828385 5%, #373737 10%,   #828385 31%, #3B3C3E 100%); */
	/* background: -moz-linear-gradient(left, #828385 0%, #828385 2%, #828385 3%, #828385 5%, #373737 10%,   #828385 31%, #2C2C2E 100%); */
	
	 Chrome10+,Safari5.1+ */
	background: -webkit-gradient(linear, left top, left bottom, 
		color-stop(0%,#828385), color-stop(2%,#828385), color-stop(3%,#828385), color-stop(5%,#828385),
		color-stop(10%,#373737),color-stop(31%,#828385), color-stop(100%,#3B3C3E)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(left, #828385 0%, #828385 2%, #828385 3%, #828385 5%, #373737 10%,   #828385 31%, #828385 31%, 
									#828385 75%, #373737 90%, #828385 95%,#828385 97%, #828385 98%,#828385 100%); /*
	background: -o-linear-gradient(left, #828385 0%, #828385 2%, #828385 3%, #828385 5%, #373737 10%,   #828385 31%, #3B3C3E 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(left, #828385 0%, #828385 2%, #828385 3%, #828385 5%, #373737 10%,   #828385 31%, #828385 31%, 
									#828385 75%, #373737 90%, #828385 95%,#828385 97%, #828385 98%,#828385 100%); /* IE10+ */
	/* filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='##2B2C2D', endColorstr='##2A2B2D',GradientType=1 );	*/
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3C4043', endColorstr='#1F1F1F',GradientType=0 );
	
}	


 .skin_grey_button a {  
    position: relative;  	 
    margin-top: 4px;  
    text-align: center; 
	vertical-align:middle; 
    display: block;  
    width: 100%;  
    height: 100%;  
    text-decoration: none;  
	font-family:Arial;
	font-size:14px;
    color: #fff;  
    z-index: 3;  
}  


.widget_framework {
	position:relative;
	margin: 10px auto;
}

.widget_panel_top {
	position:relative;
}

.widget_panel_top_sides {
	position:relative;
	float:left;
	width:50%;
	height:100%;
}

.panel_section1, .panel_section2 {
	float:left;
	/* overflow:auto*/ /*test*/
	white-space: nowrap;
}

.viewport_section1, .viewport_section2, .viewport_timebar2 {
	float:left;
	overflow:hidden;
	position:relative;
	
}

.scroller_section1, .scroller_section2 {
	/* float:left; */
	/* overflow:hidden; */
	position:relative;
}

.timebar_test_sleve {
	overflow:hidden;
	padding-top:0px;
}



.timebar_section1 {
	/* display:inline-block; */
	float:left;
	text-align:center;
	vertical-align:middle;
	font-family:Arial;
	font-size:140%;
	/* font-weight:bold; */
	letter-spacing:2px;
	color:#fff;
	border-bottom:1px solid #5555CC;
	border-right:1px solid #6ea7e5;	
}

.viewport_timebar2 {
	float:left;
	overflow:hidden;
	position:relative;
}

.timebar_section2 {
	float:left;
	font-family:Arial;
	padding:0px;
	border-bottom:1px solid #5555CC;
	position:relative;
}

.timebar_block {
	float:left;
	text-align:center;
	vertical-align:middle;
	font-family:Arial;
	color:#fff;
	height:100%;

	padding-top:5px;
	font-size:120%;
	border-right:1px solid #6ea7e5;
	border-bottom:1px solid #5555CC;
}

.channel_container {
	margin:0;
	padding:0;
	font-family:Arial;
}

.channel_section1 {
	font-family:Arial;
	float:left;
}

.channel_num {
	width:40%;
	height:100%;
	float:left;
}

.channel_num_inner {
	width:67%;
	height:100%;
	margin-left:33%;
	text-align:center;
	color: #FFF;
}

.channel_num_inner span {
	display:block;
	margin-top:10%;
	height:100%;
	line-height:100%;
	font-size:250%;
	font-weight:bold;
	vertical-align:middle;
}

.channel_idblock {
	width:60%;
	height:100%;
	float:left;
	color: #fff;
}

.channel_idblock_icon {
	height:70%;
}

.channel_idblock_icon img {
	height:60%;
	width:auto;
	margin-top:7%;
}

.channel_idblock_callLetters {
	height:30%;
	vertical-align:top;
}

.channel_idblock_callLetters span {
	display:block;
	line-height:75%;
	font-size:75%;
}

.channel_section2 {
	float:left;
	color:#FFF;
	font-family:Arial;
	overflow:hidden;
	white-space:nowrap;
}

.program_block {
	display:block;
	float:left;
	height:100%;
	border-right:1px solid #777;
}

.program_title {
	height:90%;
	width:80%;
	margin-left:30px;
	margin-top:8px;
}

.program_title span {
	display:block;
	width:100%;
	height:100%;
	white-space:normal;
	/* margin-top:10%; */
	/* margin-left:15px; */
	z-index:1;
}

.program_HD {
	position:relative;
	float:right;
	right:10px;
	bottom:26px;
	height:17px;
	width:28px;
	text-align:center;
	vertical-align:middle;
	/* margin-top:33%; */
	/* float:right; */
	color:#CCC;
	font-size:14px;
	border: 1px solid #CCC;
	z-index:2;
}

</style>


<script type="text/javascript">
// $(function() {
		
	
var Templates = {};
_.templateSettings = {
      interpolate : /\{\{([\s\S]+?)\}\}/g
	};


Templates.IpgTvGuideWidget = _.template(
	'<div id="{{widget_id}}" class="TvGuideWidget skin_greyish" ' +
	' style="width:{{widget_width}}; height:{{widget_height}}"> ' +
	'	{{html_subview}}' +
	'</div>'
);


Templates.Framework = _.template(
	'<div id="ipg_framework_{{vi}}" class="widget_framework skin_greyish" ' +
	' style="width:{{framework_width}}; height:{{framework_height}}"> ' +
	'	<div id="ipg_top_{{vi}}" class="widget_panel_top skin_greyish" ' +
	'    style="height:{{top_panel_height}}; width:{{framework_width}}"> ' +
	'  		<div class="widget_panel_top_sides">' +
	'		</div>' +
	'		<div class="widget_panel_top_sides">' +
	'			<div id="ipg_{{vi}}_btn_up" class="skin_grey_button rounded_tight ipg_btn_up" ' +
	'    	 	style="height:25px; width:75px; top:23%; left:40%">' +	
	'				<a>Up</a>' +
	'			</div> ' +
	'			<div id="ipg_{{vi}}_btn_left" class="skin_grey_button rounded_tight ipg_btn_left" ' +
	'    	 	style="height:25px; width:75px; top:40%; left:20%">' +	
	'				<a>Left</a>' +
	'			</div> ' +
	'			<div id="ipg_{{vi}}_btn_right" class="skin_grey_button rounded_tight ipg_btn_right" ' +
	'    	 	style="height:25px; width:75px; top:40%; left:60%">' +	
	'				<a>Right</a>' +
	'			</div> ' +
	'			<div id="ipg_{{vi}}_btn_down" class="skin_grey_button rounded_tight ipg_btn_down" ' +
	'    	 	style="height:25px; width:75px; top:57%; left:40%">' +	
	'				<a>Down</a>' +
	'			</div> ' +	
	'		</div>' +
	'	</div>' +
	'   	<div class="panel_section1" style="width:{{width_section1}}"> ' +
	'    		<div id="ipg_timebar_section1_{{vi}}" class="timebar_section1 skin_light_blue_gradient" ' +
	'    		 style="width:{{width_timebar_section1}}; height:{{timebar_height}}"> ' +
	'				Today' +	
	'			</div>'	+
	'			<div id="ipg_viewport_section1_{{vi}}" class="viewport_section1" ' +
	'    		 style="width:{{width_section1}}; height:{{scroller_height}}"> ' +			
	'				<div id="ipg_scroller_section1_{{vi}}" class="scroller_section1"' +
	'    		 	style="width:{{width_section1}}; height:{{scroller_height}}"> ' +
	'					{{html_section1}}' +
	'				</div>'	+	
	'			</div>' +	
	'		</div>'	+	
	'   	<div class="panel_section2" style="width:{{width_section2}}"> ' +
	'			<div id="ipg_viewport_timebar2_{{vi}}" class="viewport_section2" ' +
	'    		 style="width:{{width_section2}};"> ' +		
	'				{{html_timebar}}' +	
	'			</div>' +
	'			<div id="ipg_viewport_section2_{{vi}}" class="viewport_section2" ' +
	'    		 style="width:{{width_section2}}; height:{{scroller_height}}"> ' +			
	'				<div id="ipg_scroller_section2_{{vi}}" class="scroller_section2"' +
	'    			 style="width:{{schedule_width}}; height:{{scroller_height}}"> ' +
	'					{{html_section2}}' +
	'				</div>'	+	
	'			</div>' +		
	'		</div>'	+		
	'</div>'
);

Templates.Timebar2 = _.template(
	'<div id="ipg_timebar_section2_{{vi}}" class="timebar_section2 skin_light_blue_gradient" ' +
	' style="width:{{schedule_width}}; height:{{timebar_height}}"> ' +
	'	{{html_timebar_blocks}}' +
	'</div>'		
);

Templates.TimebarBlock = _.template(
	'       <div id="{{timeblock_view_id}}" class="timebar_block" style="width:{{timebar_block_width}}; height:{{timebar_height}}">' +
	'			{{display_time}}' +
	'		</div>'
);



Templates.Channel1 = _.template(
	'    <div class="channel_section1 skin_deep_blue_gradient" ' +
	'    style="width:{{width_section1}}; height:{{view_height}}"> ' +
	'       <div class="channel_num">' +
	'          <div class="channel_num_inner">' +
	'             <span>{{in_room_id}}</span>' +
	'          </div>' +
	'       </div>' +
	'       <div class="channel_idblock">' +
	'          <div class="channel_idblock_icon">' +
	'             <img src="{{image_dir}}{{icon}}"/>' +
	'          </div>' +
	'          <div class="channel_idblock_callLetters">' +
	'             <span>{{callLetters}}</span>' +
	'          </div>' +
	'       </div>' +
	'    </div>'
);

Templates.Channel2 = _.template(
	'    <div class="channel_section2 skin_grey_gradient" ' +
	'    style="width:{{schedule_width_plus}}; height:{{view_height}}"> ' +
	'    {{html_subview}}' +
	'    </div>'
);

Templates.Program = _.template(
	'       <div id="{{view_id}}" class="program_block" style="width:{{view_width}}; height:{{view_height}}">' +
	'			<div class="program_title"{{margin_adjust}}><span>{{title}}</span></div> ' +
	'			<div class="program_HD rounded_tight {{hide_class}}" ' +
	'			 style="right:{{HD_shift_right}}">HD</div>' +
	'		</div>'
);


ipgTvGuideWidget = {
	
	dateMillsec: function(szDateH) {
		var szDate = szDateH.substr(0,4) + "/" + szDateH.substr(5,2) + "/" + szDateH.substr(8);
		return new Date(szDate).getTime();
	},
	
	millsecToPixels: function(millSec,pixelsPerHour) {
		var hours = millSec / (3600 * 1000);
		return hours * pixelsPerHour;
	},
	
	calcDurationHours: function(startMillsec,endMillsec) {
		var diffMillsec = endMillsec - startMillsec;
		var divisor = 3600 * 1000;
		return diffMillsec/divisor;
	},
	 
	dimensNum: function(dimension) {
		return parseInt(dimension.substr(0,dimension.length-2));
	},
	
	dimensPx: function(dimension) {
		return dimension.substr(dimension.length-2,2);
	},
	
	padZeroes: function(n,j) {
		var szn = n.toString();
		var diff = j - szn.length;
		var zeroes = "";

		while(diff > 0) {
			zeroes += "0";
			diff--;
		}
		
		return zeroes + szn;
	},	
	
	localDisplayTime: function(milliTime) {
		var hours = milliTime.getHours();
		var minutes = milliTime.getMinutes();
		
		var suffix = "";
		if (hours > 12) {
			hours -= 12;
			suffix = "p";
		}
		minutes = ipgTvGuideWidget.padZeroes(minutes,2);
		return hours + ":" + minutes + suffix;
	}	
	
};
	

//** Model for a single tv program	
var ProgramModel = Backbone.Model.extend({
	defaults: {
		id:"0",
		in_room_id:"0",
		displayName:"Channel failed to load",
		callLetters:"NONE",
		icon:"",
		iptuner:""
	},

    initialize: function(){
    },

    parse: function (node) {
		var JSONnode = {
			progID:$(node).attr('progId'),
			chld:$(node).attr('chld'),
			startTime:$(node).attr('startTime'),
			endTime:$(node).attr('endTime'),
			title:$(node).attr('title'),
			rating:$(node).attr('rating'),
			desc:$(node).attr('desc'),
			genre:$(node).attr('genre'),
			genreGroup:$(node).attr('genreGroup'),
			hdTag:$(node).attr('hdTag')
		};		
        return JSONnode;
    },

});
//** END ProgramModel


var ProgramList = Backbone.Collection.extend({});


var ProgramView = Backbone.View.extend({
	template: Templates.Program,
	defaults: {
		container_selector:"#channel-list",
		ipg_start_time:"",
		ipg_end_time:""
	},
	
    initialize: function() {
		this.options = _.extend({}, this.defaults, this.options);
		//** tempoary until know more: hack to eliminate need for 
		//   el:'body' in new ChannelView({el:'body'})
		this.$el = $(this.options.el);
        _.bindAll(this, 'render');
    },
	
	render: function() {
		var model = this.model;
		var o = this.options;
		var ipg = ipgTvGuideWidget;
		var oc = o.channel_parms;
		var border_compensate = (oc.subview_seq > 1) ? 1 : 1;
		
		var ipg_start_time = (oc.ipg_start_time) ? ipg.dateMillsec(oc.ipg_start_time) : 0;
		var ipg_end_time = (oc.ipg_end_time) ? ipg.dateMillsec(oc.ipg_end_time) : 0;
		var startTime = ipg.dateMillsec(model.get('startTime'));
		var endTime = ipg.dateMillsec(model.get('endTime'));
		
		//** If program times extend beyond ipg schedule, display only a portion
		if (ipg_start_time && ipg_start_time > startTime) {
			startTime = ipg_start_time;
		}		
		if (ipg_end_time && ipg_end_time < endTime) {
			endTime = ipg_end_time;
		}
		//** If program times are entirely outside ipg schedule, don't display at all
		if (ipg_start_time && ipg_start_time >endTime) {
			return "";
		}	
		if (ipg_end_time && ipg_end_time < startTime) {
			return "";
		}		
		
		var durationHours = ipg.calcDurationHours(startTime,endTime);	
		pJSON = {};
		pJSON.view_id = oc.view_id + "_" + ipg.padZeroes(oc.subview_seq,3);
		pJSON.hide_class = (model.get('hdTag') == "1") ? "" : "hide";
		pJSON.view_width = (durationHours * oc.width_per_hour - border_compensate) + oc.width_units;
		pJSON.view_height = oc.view_height;
		pJSON.HD_shift_right = (durationHours >= 0.09) ? "10px" : "3px";
		
		if(durationHours > 0.25) {
			pJSON.title = model.get('title');
			pJSON.margin_adjust = "";
		}
		else {
			pJSON.title = "...";
			pJSON.margin_adjust = ' style="margin-left:11px" ';
		}
		
		
		return this.template(pJSON);
	}
	
});
//** END ProgramView



//** Model for a single channel	
var ChannelModel = Backbone.Model.extend({
	programClass: new ProgramModel(),
	// programList: new ProgramList(),
	
	defaults: {
		id:"0",
		in_room_id:"0",
		displayName:"Channel failed to load",
		callLetters:"NONE",
		icon:"",
		iptuner:""
	},
	
    initialize: function(){
    },
	
    parse: function (node) {
		var that = this;
		var JSONnode = {
			id:$(node).attr('id'),
			in_room_id:$(node).attr('num'),
			displayName:$(node).attr('displayName'),
			callLetters:$(node).attr('callLetters'),
			icon:$(node).attr('icon'),
			iptuner:$(node).attr('iptuner')
		};
		
		//** create collection of shows for this channel
		var parsed_collection = [];
		var xml_collection = $(node).find('show');
		xml_collection.each(function(index,node) {
			var JSONsubnode = that.programClass.parse(node);
			parsed_collection.push(JSONsubnode);
		});

		var programList = new ProgramList();
		programList.reset(parsed_collection);
		JSONnode.programList = programList;
				
        return JSONnode;
    },
	
});
//** END ChannelModel


var ChannelList = Backbone.Collection.extend({
	modelClass: new ChannelModel(),
	
	url: function(){
		if(this.options.test_mode == 1) {
			return this.options.url_test;
		}
		else {
			return this.options.url_core + this.options.room + "&test=1";
		}
	},
	
	defaults: {
		test_mode:0,
		room:"2010",
		url_test:"properties/AAGlobal/data/wifiGetIPGData.xml",
		url_core:"https://connect.roomlinx.com/ws_wifi/?func=wifiGetIPGData&room="
	},
	
    initialize: function(models,options){
		this.options = _.extend({}, this.defaults, options);
    },


	//** Modify Backbone fetch() method to read XML rather than JSON
    fetch: function (options) {
        options = options || {};
        options.dataType = "xml";
        return Backbone.Collection.prototype.fetch.call(this, options);
    },
	
    parse: function (data) {
		var that = this;
				
		//** build collection from XML
		var ipg_start_time = $(data).find('rmlxResponse').attr('ipg_start_time');
		var ipg_end_time = $(data).find('rmlxResponse').attr('ipg_end_time');
		var parsed_collection = [];
		var xml_collection = $(data).find('channel');
		xml_collection.each(function(index,node) {
			var JSONnode = that.modelClass.parse(node);
			JSONnode.ipg_start_time = ipg_start_time;
			JSONnode.ipg_end_time = ipg_end_time;
			parsed_collection.push(JSONnode);
		});
		
        return parsed_collection;
	}
});
//** END ChannelList


var ChannelView = Backbone.View.extend({
	template_widget: Templates.IpgTVGuideWidget,
	template_framework: Templates.Framework,
	template1: Templates.Channel1,
	template2: Templates.Channel2,	
	template_timebar: Templates.Timebar2,
	template_timebar_block: Templates.TimebarBlock,
	
	defaults: {
		test_mode:0,
		el:"body",
		container_selector:"#channel-list",
		room:"3142",
		image_dir:"images/icons/",
		pixels_per_hour: "450px",
		width_in_hours: 1.5,
		view_instance:"1",
		view_id_core:"ch",
		view_id_attr:"id",
		view_height:"52px",
		view_rows:8,
		scroll_hours:0.5,
		scroll_rows:4,
		view_padding_sides:"10px",
		view_padding_bottom:"20px",
		widget_id_core:"ipg",
		
		timebar_id_core:"tl",
		timebar_height:"32px",
		top_panel_height:"130px",
		test_button:"#load-input"
	},
	
    events: {
       	'click #load-input':  'loadDocument',
		'click .ipg_btn_right' : 'btnRight',
		'click .ipg_btn_left' : 'btnLeft',
		'click .ipg_btn_up' : 'btnUp',
		'click .ipg_btn_down' : 'btnDown'		
    },

    initialize: function() {
		var ipg = ipgTvGuideWidget;
		this.options = _.extend({}, this.defaults, this.options);
		var vi = this.options.view_instance;
		
		//** tempoary until know more: hack to eliminate need for 
		//   el:'body' in new ChannelView({el:'body'})
		this.$el = $(this.options.el);
        _.bindAll(this, 'render','timebarRender');
		
		//** set constants used for scrolling
		var o = this.options;
		this.view_instance = o.view_instance;
		this.scroll_h_delta = o.scroll_hours * ipg.dimensNum(o.pixels_per_hour);
		this.scroll_v_rows = o.scroll_rows;
		this.scroll_h_curr_pos = 0;
		this.scroll_v_curr_row_minus = 0;
    },
	
	btnRight: function(e) {
		if(!this.isCurrentInstance(e)) return;
		
		//** test if scrolling too far
		var ipg = ipgTvGuideWidget;
		var o = this.options;
		var scroller_id = "ipg_scroller_section2_" + o.view_instance;
		var timebar_id = "ipg_timebar_section2_" + o.view_instance;
		var multi_select = "#" + scroller_id + "," + "#" + timebar_id;
		var width_sched = ipg.schedule_width;
		var width_view = ipg.width_section2;
		var width_scroll = ipg.width_scroll;
		
		if (this.scroll_h_curr_pos + width_view + width_scroll > width_sched) {
			return false;
		}
		
		//** shift schedule
		this.scroll_h_curr_pos += width_scroll;
		$(multi_select).css("left","-" + this.scroll_h_curr_pos + "px");
		
	},
	
	btnLeft: function(e) {
		if(!this.isCurrentInstance(e)) return;
		
		//** test if scrolling too far
		var ipg = ipgTvGuideWidget;	
		var o = this.options;	
		var scroller_id = "ipg_scroller_section2_" + o.view_instance;
		var timebar_id = "ipg_timebar_section2_" + o.view_instance;
		var multi_select = "#" + scroller_id + "," + "#" + timebar_id;		
		var width_sched = ipg.schedule_width;
		var width_view = ipg.width_section2;
		var width_scroll = ipg.width_scroll;
		
		if (this.scroll_h_curr_pos - width_scroll < 0) {
			return false;
		}
		
		//** shift schedule
		this.scroll_h_curr_pos -= width_scroll;
		$(multi_select).css("left","-" + this.scroll_h_curr_pos + "px");
	},
	
	btnDown: function(e) {
		if(!this.isCurrentInstance(e)) return;
		
		//** test if scrolling too far
		var ipg = ipgTvGuideWidget;		
		var o = this.options;
		var scroller2_id = "ipg_scroller_section2_" + o.view_instance;
		var scroller1_id = "ipg_scroller_section1_" + o.view_instance;
		var multi_select = "#" + scroller1_id + "," + "#" + scroller2_id;		
		
		//** last scroll down may be partial to ensure that last page is full	
		var rows_remaining = this.num_channels - (this.scroll_v_curr_row_minus + o.view_rows);
		var num_rows_to_scroll = (rows_remaining > o.scroll_rows) ? o.scroll_rows : rows_remaining;
		
		if (num_rows_to_scroll <= 0) {
			return false;
		}
		
		//** shift schedule
		this.scroll_v_curr_row_minus += num_rows_to_scroll;
		var top_position = this.scroll_v_curr_row_minus * ipg.dimensNum(o.view_height);
		$(multi_select).css("top","-" + top_position + "px");
	},
	
	btnUp: function(e) {
		if(!this.isCurrentInstance(e)) return;
		
		//** test if scrolling too far
		var ipg = ipgTvGuideWidget;			
		var o = this.options;
		var scroller2_id = "ipg_scroller_section2_" + o.view_instance;
		var scroller1_id = "ipg_scroller_section1_" + o.view_instance;
		var multi_select = "#" + scroller1_id + "," + "#" + scroller2_id;		
		
		//** last scroll up may be partial to ensure that last page is full	
		if (this.scroll_v_curr_row_minus <= 0) {
			return false;
		}		
		
		var num_rows_to_scroll = (this.scroll_v_curr_row_minus > o.scroll_rows) ? o.scroll_rows : this.scroll_v_curr_row_minus;
		
		//** shift schedule
		this.scroll_v_curr_row_minus -= num_rows_to_scroll;
		var top_position = this.scroll_v_curr_row_minus * ipg.dimensNum(o.view_height);
		$(multi_select).css("top","-" + top_position + "px");
	},
	
	isCurrentInstance: function(obj) {
		var id = obj.currentTarget.id;
		var ii = id.indexOf(this.options.view_instance);
		var bCurrentInstance = (ii == -1) ? false : true;
		return bCurrentInstance;
	},

    loadDocument: function() {
		this.collection.fetch({
			success: this.render,
			/* ------------- 
			success: function(response,xhr) {
				this.render();
				var a = 1;
			},
			--------*/
			error: function(errorResponse){
				alert("error:" + errorResponse);
			}
			
		});
		// this.render();
    },
	
	frameworkRender: function(cJSON) {
		var ipg = ipgTvGuideWidget;	
		var tJSON = _.extend({},cJSON);
		var o = this.options;
		tJSON.framework_width = (ipg.dimensNum(tJSON.width_section1) + 
								ipg.dimensNum(tJSON.width_section2)) +  tJSON.width_units;
		tJSON.framework_height = (o.view_rows * ipg.dimensNum(o.timebar_height) +
			 				  ipg.dimensNum(o.top_panel_height)) +  tJSON.width_units;
		tJSON.scroller_height = o.view_rows * ipg.dimensNum(o.view_height) + tJSON.width_units;		  
		tJSON.top_panel_height = o.top_panel_height;
		tJSON.panel_height = ((1 + o.view_rows) * ipg.dimensNum(o.timebar_height)) +  tJSON.width_units;
		tJSON.width_timebar_section1 = (ipg.dimensNum(tJSON.width_section1) -1) + tJSON.width_units;
		tJSON.schedule_width = ipg.schedule_width + tJSON.width_units;	 
	
		return this.template_framework(tJSON);
	},
	
	widgetRender: function(cJSON) {
		var ipg = ipgTvGuideWidget;	
		var tJSON = _.extend({},cJSON);
		var o = this.options;
		tJSON.widget_id = o.widget_id_core + "_" + o.view_instance;
		
		
		tJSON.widget_width = (tJSON.width_section1 + tJSON.width_section2 + 
							  2 * ipg.dimensNum(o.view_padding_sides)) + tJSON.width_units;
		tJSON.widget_height = o.view_rows * ipg.dimensNum(o.view_height) +
							  ipg.dimensNum(o.timebar_height) +
			 				  ipg.dimensNum(o.top_panel_height) +
							  ipg.dimensNum(o.view_padding_bottom) +  tJSON.width_units;
	
		return this.template_widget(tJSON);
	},
	
	timebarRender: function(cJSON) {
		var ipg = ipgTvGuideWidget;	
		var tJSON = _.extend({},cJSON);
		var template_timebar = this.template_timebar;
		var template_timebar_block = this.template_timebar_block;
		var o = this.options;
		
		tJSON.width_section1 = (ipg.dimensNum(cJSON.width_section1) - 1) + cJSON.width_units;
		tJSON.html_timebar = "";
		var html_timebar_blocks = "";
		var startTime = ipg.dateMillsec(tJSON.ipg_start_time);
		var endTime = ipg.dateMillsec(tJSON.ipg_end_time);
		
		//** capture schedule's duration-in-pixels for later, to keep individual
		//   programs within bounds and to prevent scrolling too far
		var durationMillsec = endTime - startTime;
		ipg.schedule_width = ipg.millsecToPixels(durationMillsec,tJSON.width_per_hour);
		ipg.schedule_width_plus = (ipg.schedule_width + 1) + tJSON.width_units;
		tJSON.schedule_width = ipg.schedule_width + tJSON.width_units;
		tJSON.schedule_width_plus = ipg.schedule_width_plus;
		 
		//** Add time blocks in half-hour increments to timebar
		var workTime = startTime;
		
		if (endTime > startTime) {
			var subview_seq = 1;
			var border_compensate = 0;
			while (workTime < endTime) {
				var border_compensate = (subview_seq > 1) ? 1 : 1;
				tJSON.timeblock_view_id = o.timebar_id_core + "_" + o.view_instance + "_" + ipg.padZeroes(subview_seq,3);
				tJSON.timebar_block_width = (Math.round(0.5 * tJSON.width_per_hour) - border_compensate) + tJSON.width_units;
				tJSON.display_time = ipg.localDisplayTime(new Date(workTime));
				html_timebar_blocks += template_timebar_block(tJSON);
				workTime += 1800000;
				subview_seq++;
			} //**END while 	
		}

		tJSON.html_timebar_blocks = html_timebar_blocks;
		var html_timebar_view = template_timebar(tJSON);
		
		return html_timebar_view;
		
	},
	
    render: function() {
		//** assign DOM selectors and IDs according to custom convention
		var ipg = ipgTvGuideWidget;			
		var cJSON = {};
		var htmlText = "";
		var template1 = this.template1;
		var template2 = this.template2;

		var timebarRender = this.timebarRender;
		var o = this.options;
		var container_selector = o.container_selector;
		var view_id_pref = o.view_id_core + "_" + o.view_instance + "_";
		var view_id_attr = o.view_id_attr;
		var view_height_num = ipg.dimensNum(o.view_height);
		cJSON.vi = o.view_instance;
		cJSON.width_per_hour = ipg.dimensNum(o.pixels_per_hour);
		cJSON.width_units = ipg.dimensPx(o.pixels_per_hour);
		cJSON.image_dir = o.image_dir;
		cJSON.width_section1 = Math.round(cJSON.width_per_hour/2);
		cJSON.width_section2 = o.width_in_hours * cJSON.width_per_hour;
		cJSON.view_width = (cJSON.width_section1 + cJSON.width_section2) + cJSON.width_units;
		cJSON.width_section1 += cJSON.width_units;
		cJSON.width_section2 += cJSON.width_units;
		cJSON.view_height = o.view_height;
		cJSON.timebar_height = o.timebar_height;
		
		ipgTvGuideWidget.width_section2 = o.width_in_hours * cJSON.width_per_hour;
		ipgTvGuideWidget.width_scroll = o.scroll_hours * cJSON.width_per_hour;
		
		cJSON.html_section1 = "";
		cJSON.html_section2 = "";
		var ii = 0;
		
		//** loop through the channels in the collection.
		//   each channel model contains a collection of programs (shows.)
		this.collection.each(function(model) {
			var programList = model.get('programList');
			cJSON.view_id = view_id_pref + model.get(view_id_attr);
			cJSON.in_room_id = model.get('in_room_id');
			cJSON.icon = model.get('icon');
			cJSON.callLetters = model.get('callLetters');
			cJSON.ipg_start_time = model.get('ipg_start_time');			
			cJSON.ipg_end_time = model.get('ipg_end_time');
			if(cJSON.callLetters.indexOf("HD") == (cJSON.callLetters.length - 2)) {
				cJSON.callLetters = cJSON.callLetters.substr(0,cJSON.callLetters.length-2);
			}
			
			/* test mode: compensate for zero in_room channel numbers */
			if(o.test_mode) {
				if(cJSON.in_room_id == "0") {
					cJSON.in_room_id = Math.floor(Math.random() * 50) + 3;
				}
			}
			
			//** Timebar rendering and one-time processing
			if(!ii) {
				cJSON.html_timebar = timebarRender(cJSON);
				cJSON.schedule_width = ipg.schedule_width;
				cJSON.schedule_width_plus = ipg.schedule_width_plus;			
			}
			
			//** Add a view for each program (show), all within one row for the channel
			var html_subview = "";			
			cJSON.subview_seq = 1;
			programList.each(function(pgm) {
				var subview = new ProgramView({model:pgm,channel_parms:cJSON});
				html_subview += subview.render();
				cJSON.subview_seq++;
			}); //**END each programList
			
			//** add a channel with its programs
			cJSON.html_subview = html_subview;
			cJSON.html_section1 += template1(cJSON);			
			cJSON.html_section2 += template2(cJSON);			
			ii++;
	        
		}); //**END each collection
		
		this.num_channels = ii;
		
		//** create html for the framework with all the sub-views, i.e. the entire widget
		htmlText = this.frameworkRender(cJSON);

		$(container_selector).append(htmlText);
		return this;
    },


});
//** END ChannelView
$(document).ready(function(){
	
	// var programModel = new ProgramModel();
	// var programList = new ProgramList({model:programModel});
	
	// var channelModel = new ChannelModel();
	var channelList = new ChannelList([],{test_mode:1});
	
	var view = new ChannelView({collection:channelList, 
		container_selector:'#channel-list',
		view_rows:8, 
		scroll_rows:4,
		width_in_hours:1.5,	
		pixels_per_hour: "450px",
		view_height:"52px",
		timebar_height:"32px",	
		test_mode:1});
	
	view.loadDocument();
	
});
// }); //**END JQuery wrapper
</script>
</head>
<body>
<button id="load-input">Load Channels</button>


<div id="channel-list" class="container rounded skin-whitish">
	
</div>

</body>
</html>