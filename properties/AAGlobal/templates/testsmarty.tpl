<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name=apple-mobile-web-app-capable content=yes>
	<meta name=apple-mobile-web-app-status-bar-style content=black>
    <title>Roomlinx Test</title>
    <link rel="stylesheet" href="lib/org/jquery.mobile/jquery.mobile-1.2.0.css" />
    <script type="text/javascript" src="lib/org/jquery/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="lib/org/jquery.mobile/jquery.mobile-1.2.0.js"></script>
	
	<!-- ><link rel="stylesheet" href="jquery.mobile.1.0.css" /> -->
    
    <!-- Custom style fixes for JQM bugs -->
    <style>
		.cust-ui-li-aside {
			width:15%;
			margin: 0.7em 3em;	
		}
	
		.ui-body-c {
			background: #111;
			color:#fff;
		}	
		
		/** Override JQM's bloody severe margins in header H1's **/
		.ui-header .ui-title {
		    margin-right: 0%;
		    margin-left: 0%;
			/* overflow:hidden; */
			/* white-space:ellipsis; */
		}			
		
		#top_header_content {
			display-block;
			width:1003px;
			position:relative;
			margin: 0  auto;
			overflow-x:hidden;
			white-space:nowrap;
			vertical-align:middle;
		}
		
		#h1-text {
			float: left;
			color:#fff;
			font-family:Arial;
			font-size:1.5em;
			font-weight:normal;
			line-height:2em;
			text-transform:uppercase;
			
			/* YMW text-model experiment */
			font-size:24px;
			line-height:48px;
		}
		
		#h1-text.mobile {
			/* margin-top:0.75em; */
			font-size:10px;
			line-height:24px;
		}
		
		#h1-img {
			/** margin:10px 20px 0 35px; **/
			margin-left:35px;
			margin-right:20px;
			float:left;
			border:none;
			height: 3em;
			width:auto;
			
			/* YMW text-model experiment */
			height: 48px;
			
		}
		
		#h1-img.mobile {
			margin-left:10px;
			margin-right:10px;
			height: 24px;
			width:auto;
		}
		
		/** Diagnostics **/
		.ix-iphone-port, .ix-iphone-land,
		.ix-ipad-port, .ix-ipad-land,
		.ix-desktop-port, .ix-desktop-land,
		.ix-desktop-narrow {
			display:none;
			float: left;
			color:#fff;
			font-family:Arial;
			font-size:1.5em;
			font-weight:normal;
			line-height:3em;
			text-transform:uppercase;
		}


	
			
		
		
			.ui-bar-b {
				border: 1px solid #4d4d4d;
				/* Firefox */
				background: -moz-linear-gradient(top,#222222 50%,#1b1b1b 100%);
			
				/* Safari and Chrome */
				background: -webkit-linear-gradient(top,#222222 50%,#1b1b1b 100%);
			
				/* IE */
				*filter: progid:DXImageTransform.Microsoft.gradient(gradientType=1,startColorstr='#000000', endColorstr='#000000');
			}

			/* .ui-bar-a{ */
			#top_header {
				color: #000;
				/* Firefox */
				background: -moz-linear-gradient(top,#000000 25%,#111111 37%,#232323 50%,#151515 75%,#000000 100%);
			
				/* Safari and Chrome */
				background: -webkit-linear-gradient(top,#000000 25%,#111111 37%,#232323 50%,#151515 75%,#000000 100%);
			
				/* IE */
				*filter: progid:DXImageTransform.Microsoft.gradient(gradientType=1,startColorstr='#000000', endColorstr='#000000');
				text-align: center;
			}			
			
		
		.ui-bar-a {
			background: #000;
			color:#ffffff;
			border-bottom: 1px solid #000000;
			
		}
		
		.ui-btn-text * {
			color:#FFFFFF;
			text-shadow:0 1px 0 #000;
		}
		
	</style>

	
</head>

<body>

	<div data-role="page" id="page1">
    	<div id="top_header" data-role="header" data-theme="a">
			<h1>
				<div id="top_header_content">
					<img id="h1-img" border="0" title="Hyatt" alt="Hyatt" src="image/logo-hyattregency5.png"/>
		        	<span id="h1-text">welcome to hyatt regency houston</span>
				</div>
			</h1>
			
        </div>
        
        <!-- Content -->
        <div data-role="content">

		<ul data-role="listview" data-inset="true" data-theme="a" data-corners="false" data-shadow="false">
			
			
        	<li data-role="list-divider" data-corners="false" >Test list loaded from Smarty</li>
			<li>	
				<div class="ui-grid-c">
				
					{foreach from=$packages item=package}
					    <div class="ui-block-a">{$package.id}</div>
					    <div class="ui-block-b">{$package.description}</div>
					    <div class="ui-block-c">{$package.price}</div>
					{/foreach}

			 	</div>
			</li>
			
		</ul>
		
        </div>
        <!-- End Content -->
        
        <div data-role="footer" data-position="fixed">
        	<h4>Here's some footer content</h4>
         </div>
    </div> 
    
</body>

<script type="text/javascript">
	/**	Force mobile device page header elements to smaller sizes **/ 
	objUserAgent = navigator.userAgent;
	// $('#h1-text').text(objUserAgent);
	if(objUserAgent.search('iPhone') != -1) {
	// if(objUserAgent.indexOf('iPhone') != -1) {
		$('#top_header_content').children().addClass('mobile');
	}
</script>	

</html>
    
