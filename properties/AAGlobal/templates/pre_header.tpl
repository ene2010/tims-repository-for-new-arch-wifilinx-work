<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name=apple-mobile-web-app-capable content=yes>
	<meta name=apple-mobile-web-app-status-bar-style content=black>
    <title>Roomlinx Test</title>
    <link rel="stylesheet" href="lib/org/jquery.mobile/jquery.mobile-1.2.0.css" />
	<link rel="stylesheet" href="lib/rmlx/mobile/css/roomlinx-jquery-mobile-1.css" />
    <script type="text/javascript" src="lib/org/jquery/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="lib/org/jquery.mobile/jquery.mobile-1.2.0.js"></script>
