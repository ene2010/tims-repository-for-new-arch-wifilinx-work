{if $debug_on}
<div data-role="page" id="debugging">
<ul data-role="listview" data-inset="true" data-theme="a" data-corners="false" data-shadow="false">
	<li data-role="list-divider" data-corners="false" >Debugging</li>
	<li>	
		<div class="ui-grid-b">		
			{foreach from=$debugArray item=debugItem}
			    <div class="ui-block-a">{$debugItem.var_name}</div>
			    <div class="ui-block-b">{$debugItem.var_value}</div>
			{/foreach}

	 	</div>
	</li>
</ul>
</div>
{/if}