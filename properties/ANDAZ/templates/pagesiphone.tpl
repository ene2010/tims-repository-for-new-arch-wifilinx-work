	
	<div data-role="page" id="start">
		<!-- might want more room for body -->
		<!--
	   	<div id="top_header" data-role="header" data-theme="a">
			<h1>{$deviceClass}</h1>
		</div>
		-->
		
		<div data-role="content" class="border_body rounded {$deviceClass}">
			<div class="ui-grid-activator-a">
				<div class="ui-block-activator-a">
					<img class="{$deviceClass}" src="images/logo_header.png">
					<img class="{$deviceClass}" src="images/logo_main-24.png">
				</div>
				<div class="ui-block-activator-b">
					<h6 class="welcome ui-hide">Welcome to Andaz 5th Avenue</h6>
					<a href="#login" data-role="button" data-inline="true" class="ui-btn-spanner {$deviceClass}" 
						data-transition="slide" data-direction="forward"  data-theme="d">Internet</a>
					<a href="#" data-role="button" data-inline="true" class="ui-btn-spanner {$deviceClass}" data-theme="d">Services</a>
				</div>
			</div>
		</div>
		<!-- End Content ---->
		
		<!--
        <div data-role="footer" data-position="fixed">
        	<h4>Here's some footer content</h4>
         </div>
		-->
    </div> 
	<!-- END start page -->

	<div data-role="page" id="login">
		<div data-role="content" class="border_body rounded {$deviceClass}">
			<div class="ui-grid-activator-a">
				<div class="ui-block-activator-a">
					<img class="{$deviceClass}" src="images/logo_header.png">
				</div>
				<div class="ui-block-activator-b">
					<h6 class="welcome">Welcome to Andaz 5th Avenue</h6>
					<a href="#" data-role="button" data-inline="true" class="ui-btn-spanner {$deviceClass}" data-theme="d">Internet</a>
					<a href="#" data-role="button" data-inline="true" class="ui-btn-spanner {$deviceClass}" data-theme="d">Services</a>
				</div>
			</div>
		</div>
		<!-- End Content ---->

	</div>    