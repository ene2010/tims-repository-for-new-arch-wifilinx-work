<link rel="stylesheet" href="lib/rmlx/mobile/css/roomlinx-mobile.css" />
<style>
</style>

<script type="text/javascript">

//** Force the hiding of the address bar after orientation change,
//   then trigger an event signaling resizing of the window
//   (overcome a problem with heights failing to resize properly).
$(window).on("orientationchange", function(event) {
	//** Hide address bar and trigger resize
    setTimeout(function(){
         window.scrollTo(0, 1);
		 $(window).trigger("throttledresize");
     }, 0);
	 
	 reorientFormat(event.orientation);
	 
});

//** Swipe to return to prior page
$("#login").live("swiperight",function(){	
	$.mobile.changePage("#start",{
		transition:"slide", reverse: true
	});
});

function reorientFormat(orientation){
	
	if(orientation == "landscape") {
		$(".welcome.ui-hide").removeClass("ui-hide").addClass("ui-show");
		$(".ui-grid-activator-a").addClass("ui-grid-a");
		$(".ui-block-activator-a").addClass("ui-block-a");
		$(".ui-block-activator-b").addClass("ui-block-b");
	}
	else {
		$(".welcome.ui-show").removeClass("ui-show").addClass("ui-hide");
		$(".ui-grid-activator-a").removeClass("ui-grid-a");
		$(".ui-block-activator-a").removeClass("ui-block-a");
		$(".ui-block-activator-b").removeClass("ui-block-b");
	}
	
	return false;
	
}


</script>

</head>

<body>
	