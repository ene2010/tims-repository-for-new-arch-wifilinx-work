<?php
/*
* @script: always execute at the beginning of request processing
*/


function deviceDetect($device_class_map){
	$deviceClass = "";
	
	$userAgent = $_SERVER['HTTP_USER_AGENT'];

	if(strpos($userAgent,"iPad") !== false) {
		$deviceClass = "ipad";
	}
	else if(strpos($userAgent,"iPhone") !== false) {
		$deviceClass = "iphone";
	}
	
	//** Override device class if requested in query string
	$deviceClass = !empty($_REQUEST['devicemode']) ? $_REQUEST['devicemode'] : $deviceClass;
	return $deviceClass;
	
}

function setDebug() {
	global $Gsmarty;
	$Gsmarty->assign("debug_on",false);
	
	if(empty($_REQUEST['showdebug'])) {
		return false;
	}

	$Gsmarty->assign("debug_on",true);
	addDebug("userAgent",$_SERVER['HTTP_USER_AGENT']);
}

function addDebug($name,$value){
	global $Gsmarty;
	$debugArray = array("var_name"=>$name,"var_value"=>$value);
	$Gsmarty->append("debugArray",$debugArray);	
}

?>
