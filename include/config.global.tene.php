<?php
/**
 * @script: global config file for mobile tene demo
 */
require_once(dirname(__FILE__) . "/always.global.php");

//** Property-level definitions
$global_set			= "AAGlobal";		// global templates
$Pcode				= "ANDAZ";			// spirit code for property

//** Device userAgent to CSS class definitions 
$device_class_map = Array(""=>"","iPhone"=>"iphone","iPad"=>"ipad");
$deviceClass = deviceDetect($device_class_map);

//** Implement Smarty for MAMP environment
define('SMARTY_DIR', '/usr/local/lib/smarty-3.1.14/libs/');
$portal_base 	    = "/Applications/MAMP/htdocs/roomlinx";
$global_base 		= "$portal_base/properties/$global_set";
$property_base 		= "$portal_base/properties/$Pcode";
$smarty_work		= "$portal_base/work_smarty";

require_once(SMARTY_DIR . "Smarty.class.php");
$Gsmarty = new Smarty();
$Gsmarty->template_dir  = ''.$global_base.'/templates';
$Gsmarty->compile_dir   = ''.$smarty_work.'/templates_c';
$Gsmarty->cache_dir     = ''.$smarty_work.'/cache';
$Gsmarty->config_dir    = ''.$smarty_work.'/configs';
$Gsmarty->plugins_dir   = SMARTY_DIR . 'plugins';
$Gsmarty->caching       = false;
$Gsmarty->force_compile = true;
$Gsmarty->debugging     = false;
$Gsmarty->assign("deviceClass", $deviceClass);

$Psmarty = new Smarty();
$Psmarty->template_dir  = ''.$property_base.'/templates';
$Psmarty->compile_dir   = ''.$smarty_work.'/templates_c';
$Psmarty->cache_dir     = ''.$smarty_work.'/cache';
$Psmarty->config_dir    = ''.$smarty_work.'/configs';
$Psmarty->plugins_dir   = SMARTY_DIR . 'plugins';
$Psmarty->caching       = false;
$Psmarty->force_compile = true;
$Psmarty->debugging     = false;
$Psmarty->assign("deviceClass", $deviceClass);

//** Set debugging true/false for display of debugging template
setDebug();
	
?>