<!DOCTYPE HTML>
<html>
<head>
<title>Roomlinx Backbone Test</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<!--
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.cdnjs.com/ajax/libs/underscore.js/1.1.4/underscore-min.js"></script>
<script type="text/javascript" src="http://ajax.cdnjs.com/ajax/libs/backbone.js/0.3.3/backbone-min.js"></script>

<script type="text/javascript" src="http://ajax.cdnjs.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
-->
<script type="text/javascript" src="/lib/org/underscore/underscore-min.js"></script>
<script type="text/javascript" src="/lib/org/backbone/backbone.js"></script>
<!-- ><script type="text/javascript" src="lib/rmlx/structr/structr.js"></script> -->

<style>
.rounded  {
	-moz-border-radius: 10px;
	-webkit-border-radius: 10px;
	border-radius: 10px;
	-moz-box-shadow: 	4px 4px 4px #888;
	-webkit-box-shadow: 0px 4px 4px #888;
	box-shadow: 	4px 4px 4px #888;
}

.rounded_tight  {
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;
}


.hide {
	display:none;
}

.skin_whitish {
	background: #EEE;
	border: 1px solid #000;	
	color: #000;
}

.skin_greyish {
	background: #222;
	border: none;
	color: #FFF;
}

.skin_clear {
	border-bottom: 1px solid #000;
}

.skin_grey_gradient {
	background: -moz-linear-gradient(top, #3C4043 0%, #3D3C3A 20%, #323232 40%, #2E2E2E 60%, #242424 80%, #1F1F1F 100%); /* FF3.6+ */
	background: -webkit-linear-gradient(top, #3C4043 0%, #3D3C3A 20%, #323232 40%, #2E2E2E 60%, #242424 80%, #1F1F1F 100%); /* Chrome10+,Safari5.1+ */
	background: -webkit-gradient(linear, left top, left bottom, 
		color-stop(0%,#3C4043), color-stop(20%,#3D3C3A), color-stop(40%,#323232),
		color-stop(60%,#2E2E2E), color-stop(80%,#242424), color-stop(100%,#1F1F1F)); /* Chrome,Safari4+ */
	background: -o-linear-gradient(top, #3C4043 0%, #3D3C3A 20%, #323232 40%, #2E2E2E 60%, #242424 80%, #1F1F1F 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #3C4043 0%, #3D3C3A 20%, #323232 40%, #2E2E2E 60%, #242424 80%, #1F1F1F 100%); /* IE10+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3C4043', endColorstr='#1F1F1F',GradientType=0 );
}

.skin_deep_blue_gradient {
    background: -moz-linear-gradient(top, #212E44 0%, #1E2A3C 20%, #1B2534 40%, #09101A 60%, #080E15 80%, #080C11 100%); /* FF3.6+ */
	background: -webkit-linear-gradient(top, #212E44 0%, #1E2A3C 20%, #1B2534 40%, #09101A 60%, #080E15 80%, #080C11 100%); /* Chrome10+,Safari5.1+ */
	background: -webkit-gradient(linear, left top, left bottom, 
		color-stop(0%,#212E44), color-stop(20%,#1E2A3C), color-stop(40%,#1B2534),
		color-stop(60%,#09101A), color-stop(80%,#080E15), color-stop(100%,#080C11)); /* Chrome,Safari4+ */
	background: -o-linear-gradient(top, #212E44 0%, #1E2A3C 20%, #1B2534 40%, #09101A 60%, #080E15 80%, #080C11 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #212E44 0%, #1E2A3C 20%, #1B2534 40%, #09101A 60%, #080E15 80%, #080C11 100%); /* IE10+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#212E44', endColorstr='#080C11',GradientType=0 );
}

.skin_light_blue_gradient {
    background: -moz-linear-gradient(top, #3165A6 0%, #2A6099 20%, #295A91 40%, #224774 60%, #1D3B63 80%, #1B3152 100%); /* FF3.6+ */
	background: -webkit-linear-gradient(top, #3165A6 0%, #2A6099 20%, #295A91 40%, #224774 60%, #1D3B63 80%, #1B3152 100%); /* Chrome10+,Safari5.1+ */
	background: -webkit-gradient(linear, left top, left bottom, 
		color-stop(0%,#3165A6), color-stop(20%,#2A6099), color-stop(40%,#295A91),
		color-stop(60%,#224774), color-stop(80%,#1D3B63), color-stop(100%,#1B3152)); /* Chrome,Safari4+ */
	background: -o-linear-gradient(top, #3165A6 0%, #2A6099 20%, #295A91 40%, #224774 60%, #1D3B63 80%, #1B3152 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #3165A6 0%, #2A6099 20%, #295A91 40%, #224774 60%, #1D3B63 80%, #1B3152 100%); /* IE10+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3165A6', endColorstr='#1B3152',GradientType=0 );
}	

.container {
	/* width:980px; */
	width:1300px;
	margin: 0 auto;
	padding: 20px;
}

.timebar_test_sleve {
	overflow:hidden;
	padding-top:0px;
}

.timebar_section1 {
	/* display:inline-block; */
	float:left;
	text-align:center;
	vertical-align:middle;
	font-family:Arial;
	font-size:140%;
	/* font-weight:bold; */
	letter-spacing:2px;
	color:#fff;
	border-bottom:1px solid #777;
	border-right:1px solid #6ea7e5;	
}

.timebar_section2 {
	float:left;
	font-family:Arial;
	padding:0px;
	/* border-bottom:1px solid #777; */
	overflow:hidden;
}

.timebar_block {
	float:left;
	text-align:center;
	vertical-align:middle;
	font-family:Arial;
	color:#fff;
	height:100%;

	padding-top:5px;
	font-size:120%;
	border-right:1px solid #6ea7e5;
	border-bottom:1px solid #5555CC;
}

.channel_container {
	margin:0;
	padding:0;
	font-family:Arial;
}

.channel_section1 {
	float:left;
}

.channel_num {
	width:40%;
	height:100%;
	float:left;
}

.channel_num_inner {
	width:67%;
	height:100%;
	margin-left:33%;
	text-align:center;
	color: #FFF;
}

.channel_num_inner span {
	display:block;
	margin-top:10%;
	height:100%;
	line-height:100%;
	font-size:250%;
	font-weight:bold;
	vertical-align:middle;
}

.channel_idblock {
	width:60%;
	height:100%;
	float:left;
	color: #fff;
}

.channel_idblock_icon {
	height:70%;
}

.channel_idblock_icon img {
	height:60%;
	width:auto;
	margin-top:7%;
}

.channel_idblock_callLetters {
	height:30%;
	vertical-align:top;
}

.channel_idblock_callLetters span {
	display:block;
	line-height:75%;
	font-size:75%;
}

.channel_section2 {
	float:left;
	color:#FFF;
	overflow:hidden;
	white-space:nowrap;
}

.program_block {
	display:block;
	float:left;
	height:100%;
	border-right:1px solid #777;
}

.program_title {
	height:90%;
	width:80%;
	margin-left:30px;
	margin-top:8px;
}

.program_title span {
	display:block;
	width:100%;
	height:100%;
	white-space:normal;
	/* margin-top:10%; */
	/* margin-left:15px; */
	z-index:1;
}

.program_HD {
	position:relative;
	float:right;
	right:10px;
	bottom:26px;
	height:17px;
	width:28px;
	text-align:center;
	vertical-align:middle;
	/* margin-top:33%; */
	/* float:right; */
	color:#CCC;
	font-size:14px;
	border: 1px solid #CCC;
	z-index:2;
}



</style>


<script type="text/javascript">
// $(function() {
	
var Templates = {};
_.templateSettings = {
      interpolate : /\{\{([\s\S]+?)\}\}/g
	};


Templates.TvGuideWidget = _.template(
	'<div id="{{widget_id}}" class="TvGuideWidget skin_greyish" ' +
	' style="width:{{widget_width}}; height:{{widget_height}}"> ' +
	'	{{html_subview}}' +
	'</div>'
);


Templates.Framework = _.template(
	'<div id="{{framework_id}}" class="widget_framework skin_greyish" ' +
	' style="width:{{framework_width}}; height:{{framework_height}}"> ' +
	'	<div id="{{panel_top_id}}" class="widget_panel_top skin_greyish" ' +
	'    style="height:{{top_panel_height}}"> ' +
	'   	<div class="panel_section1" style="width:{{width_section1}}"> ' +
	'    		<div id="{{timebar_section1_id}}" class="timebar_section1 skin_light_blue_gradient" ' +
	'    		 style="width:{{width_section1}}; height:{{timebar_height}}"> ' +
	'				Today' +	
	'			</div>'	+		
	'			<div id="{{scroller_section1_id}}" class="scroller_section1"' +
	'    		 style="width:{{width_section1}}; height:{{scroller_height}}"> ' +
	'				{{html_section1}}' +
	'			</div>'	+		
	'		</div>'	+	
	'   	<div class="panel_section2" style="width:{{width_section2}}"> ' +
	'    		<div id="{{timebar_section2_id}}" class="timebar_section2 skin_light_blue_gradient" ' +
	'    		 style="width:{{width_section2}}; height:{{timebar_height}}"> ' +
	'				{{html_timebar}}' +	
	'			</div>'	+		
	'			<div id="{{scroller_section2_id}}" class="scroller_section2"' +
	'    		 style="width:{{width_section2}}; height:{{scroller_height}}"> ' +
	'				{{html_section2}}' +
	'			</div>'	+			
	'		</div>'	+		
	'	</div>'	+
	'	{{html_subview}}' +
	'</div>'
);


Templates.TimebarTest = _.template(
	'<div class="timebar_test_sleve" ' +
	' style="width:{{view_width}}; height:{{timebar_height}}"> ' +	
	'<div id="{{timebar_section1_id}}" class="timebar_section1 skin_light_blue_gradient" ' +
	' style="width:{{width_section1}}; height:{{timebar_height}}"> ' +
	'	Today' +	
	'</div>' +	
	'<div id="{{timebar_section2_id}}" class="timebar_section2 skin_light_blue_gradient" ' +
	' style="width:{{width_section2}}; height:{{timebar_height}}"> ' +
	'	{{html_timebar_blocks}}' +	
	'</div>' +
	'</div>'		
);


Templates.TimebarBlock = _.template(
	'       <div id="{{timeblock_view_id}}" class="timebar_block" style="width:{{timebar_block_width}}; height:{{timebar_height}}">' +
	'			{{display_time}}' +
	'		</div>'
);


Templates.Channel = _.template(
	'<div id="{{view_id}}" class="channel_container skin_clear" ' +
	' style="width:{{view_width}}; height:{{view_height}}"> ' +
	'    <div class="channel_section1 skin_deep_blue_gradient" ' +
	'    style="width:{{width_section1}}; height:{{view_height}}"> ' +
	'       <div class="channel_num">' +
	'          <div class="channel_num_inner">' +
	'             <span>{{in_room_id}}</span>' +
	'          </div>' +
	'       </div>' +
	'       <div class="channel_idblock">' +
	'          <div class="channel_idblock_icon">' +
	'             <img src="{{image_dir}}{{icon}}"/>' +
	'          </div>' +
	'          <div class="channel_idblock_callLetters">' +
	'             <span>{{callLetters}}</span>' +
	'          </div>' +
	'       </div>' +
	'    </div>' +
	'    <div class="channel_section2 skin_grey_gradient" ' +
	'    style="width:{{width_section2}}; height:{{view_height}}"> ' +
	'    {{html_subview}}' +
	'    </div>' +
	'</div>'
);

Templates.Program = _.template(
	'       <div id="{{view_id}}" class="program_block" style="width:{{view_width}}; height:{{view_height}}">' +
	'			<div class="program_title"{{margin_adjust}}><span>{{title}}</span></div> ' +
	'			<div class="program_HD rounded_tight {{hide_class}}" ' +
	'			 style="right:{{HD_shift_right}}">HD</div>' +
	'		</div>'
);
	

//** Model for a single tv program	
var ProgramModel = Backbone.Model.extend({
	defaults: {
		id:"0",
		in_room_id:"0",
		displayName:"Channel failed to load",
		callLetters:"NONE",
		icon:"",
		iptuner:""
	},

    initialize: function(){
    },

    parse: function (node) {
		var JSONnode = {
			progID:$(node).attr('progId'),
			chld:$(node).attr('chld'),
			startTime:$(node).attr('startTime'),
			endTime:$(node).attr('endTime'),
			title:$(node).attr('title'),
			rating:$(node).attr('rating'),
			desc:$(node).attr('desc'),
			genre:$(node).attr('genre'),
			genreGroup:$(node).attr('genreGroup'),
			hdTag:$(node).attr('hdTag')
		};		
        return JSONnode;
    },

	/*-------------------------
    render: function(model) {
        $("#channel-list").append("<li>"
			+ model.get("id") + "  "
			+ model.get("in_room_id") + "  "
			+ model.get("displayName") + "  "
			+"</li>");
		return this;
    },
	---------------------*/

});
//** END ProgramModel


var ProgramList = Backbone.Collection.extend({});


var ProgramView = Backbone.View.extend({
	template: Templates.Program,
	defaults: {
		container_selector:"#channel-list",
		ipg_start_time:"",
		ipg_end_time:""
	},
	
    initialize: function() {
		this.options = _.extend({}, this.defaults, this.options);
		//** tempoary until know more: hack to eliminate need for 
		//   el:'body' in new ChannelView({el:'body'})
		this.$el = $(this.options.el);
        _.bindAll(this, 'render');
    },
	
	padZeroes: function(n,j) {
		var szn = n.toString();
		var diff = j - szn.length;
		var zeroes = "";

		while(diff > 0) {
			zeroes += "0";
			diff--;
		}
		
		return zeroes + szn;
	},
	
	calcDurationHours: function(startMillsec,endMillsec) {
		var diffMillsec = endMillsec - startMillsec;
		var divisor = 3600 * 1000;
		return diffMillsec/divisor;
	},
	
	dateMillsec: function(szDateH) {
		var szDate = szDateH.substr(0,4) + "/" + szDateH.substr(5,2) + "/" + szDateH.substr(8);
		return new Date(szDate).getTime();
	},
	
	dimensNum: function(dimension) {
		return dimension.substr(0,dimension.length-2);
	},
	
	dimensPx: function(dimension) {
		return dimension.substr(dimension.length-2,2);
	},
		
	
	render: function() {
		var model = this.model;
		var o = this.options;
		var oc = o.channel_parms;
		var border_compensate = (oc.subview_seq > 1) ? 1 : 1;
		var ipg_start_time = (oc.ipg_start_time) ? this.dateMillsec(oc.ipg_start_time) : 0;
		var ipg_end_time = (oc.ipg_end_time) ? this.dateMillsec(oc.ipg_end_time) : 0;
		var startTime = this.dateMillsec(model.get('startTime'));
		var endTime = this.dateMillsec(model.get('endTime'));
		if (ipg_start_time && ipg_start_time > startTime) {
			startTime = ipg_start_time;
		}		
		if (ipg_end_time && ipg_end_time < endTime) {
			endTime = ipg_end_time;
		}
		
		var durationHours = this.calcDurationHours(startTime,endTime);
				
		pJSON = {};
		pJSON.view_id = oc.view_id + "_" + this.padZeroes(oc.subview_seq,3);
		pJSON.hide_class = (model.get('hdTag') == "1") ? "" : "hide";
		pJSON.view_width = (durationHours * oc.width_per_hour - border_compensate) + oc.width_units;
		pJSON.view_height = oc.view_height;
		pJSON.HD_shift_right = (durationHours >= 0.09) ? "10px" : "3px";
		// pJSON.view_height = (this.dimensNum(oc.view_height) + 1) + this.dimensPx(oc.view_height);
		
		if(durationHours > 0.25) {
			pJSON.title = model.get('title');
			pJSON.margin_adjust = "";
		}
		else {
			pJSON.title = "...";
			pJSON.margin_adjust = ' style="margin-left:11px" ';
		}
		
		
		return this.template(pJSON);
	}
	
});
//** END ProgramView



//** Model for a single channel	
var ChannelModel = Backbone.Model.extend({
	programClass: new ProgramModel(),
	// programList: new ProgramList(),
	
	defaults: {
		id:"0",
		in_room_id:"0",
		displayName:"Channel failed to load",
		callLetters:"NONE",
		icon:"",
		iptuner:""
	},
	
    initialize: function(){
    },
	
    parse: function (node) {
		var that = this;
		var JSONnode = {
			id:$(node).attr('id'),
			in_room_id:$(node).attr('num'),
			displayName:$(node).attr('displayName'),
			callLetters:$(node).attr('callLetters'),
			icon:$(node).attr('icon'),
			iptuner:$(node).attr('iptuner')
		};
		
		//** create collection of shows for this channel
		var parsed_collection = [];
		var xml_collection = $(node).find('show');
		xml_collection.each(function(index,node) {
			var JSONsubnode = that.programClass.parse(node);
			parsed_collection.push(JSONsubnode);
		});

		var programList = new ProgramList();
		programList.reset(parsed_collection);
		JSONnode.programList = programList;
				
        return JSONnode;
    },
	
	/*----------------------------
    render: function(model) {
        $("#channel-list").append("<li>"
			+ model.get("id") + "  "
			+ model.get("in_room_id") + "  "
			+ model.get("displayName") + "  "
			+"</li>");
		return this;
    },
	-----------------------*/
	
});
//** END ChannelModel


var ChannelList = Backbone.Collection.extend({
	modelClass: new ChannelModel(),
	url:"properties/AAGlobal/data/wifiGetIPGData.xml",
	
    initialize: function(){
    },
	


	//** Modify Backbone fetch() method to read XML rather than JSON
    fetch: function (options) {
        options = options || {};
        options.dataType = "xml";
        return Backbone.Collection.prototype.fetch.call(this, options);
    },
	
    parse: function (data) {
		var that = this;
		var ipg_start_time = $(data).find('rmlxResponse').attr('ipg_start_time');
		var ipg_end_time = $(data).find('rmlxResponse').attr('ipg_end_time');
		var parsed_collection = [];
		var xml_collection = $(data).find('channel');
		xml_collection.each(function(index,node) {
			var JSONnode = that.modelClass.parse(node);
			JSONnode.ipg_start_time = ipg_start_time;
			JSONnode.ipg_end_time = ipg_end_time;
			parsed_collection.push(JSONnode);
		});
		
        return parsed_collection;
	}
});
//** END ChannelList


var ChannelView = Backbone.View.extend({
	template: Templates.Channel,
	template_inner: Templates.Program,
	template_timebar: Templates.TimebarTest,
	template_timebar_block: Templates.TimebarBlock,
	
	defaults: {
		test_mode:0,
		el:"body",
		container_selector:"#channel-list",
		image_dir:"images/icons/",
		pixels_per_hour: "450px",
		width_in_hours: 2,
		view_instance:"1",
		view_id_core:"ch",
		view_id_attr:"id",
		view_height:"52px",
		timebar_id_core:"tl",
		timebar_height:"32px"
	},
	
    events: {
       	'click #load-input':  'loadDocument',
    },

    initialize: function() {
        // this.channelList = new ChannelList();
		// this.collection = this.channelList;
		this.options = _.extend({}, this.defaults, this.options);
		
		//** tempoary until know more: hack to eliminate need for 
		//   el:'body' in new ChannelView({el:'body'})
		this.$el = $(this.options.el);
        _.bindAll(this, 'render','timebarRender');
    },

    loadDocument: function() {
		// this.channelList.fetch();
		this.collection.fetch({
			success: this.render,
			/* ------------- 
			success: function(response,xhr) {
				this.render();
				var a = 1;
			},
			--------*/
			error: function(errorResponse){
				alert("error:" + errorResponse);
			}
			
		});
		// this.render();
    },
	
	padZeroes: function(n,j) {
		var szn = n.toString();
		var diff = j - szn.length;
		var zeroes = "";

		while(diff > 0) {
			zeroes += "0";
			diff--;
		}
		
		return zeroes + szn;
	},
	
	dateMillsec: function(szDateH) {
		var szDate = szDateH.substr(0,4) + "/" + szDateH.substr(5,2) + "/" + szDateH.substr(8);
		return new Date(szDate).getTime();
	},
	
	localDisplayTime: function(milliTime) {
		var hours = milliTime.getHours();
		var minutes = milliTime.getMinutes();
		
		var suffix = "";
		if (hours > 12) {
			hours -= 12;
			suffix = "p";
		}
		minutes = this.padZeroes(minutes,2);
		return hours + ":" + minutes + suffix;
	},
	
	dimensNum: function(dimension) {
		return dimension.substr(0,dimension.length-2);
	},
	
	dimensPx: function(dimension) {
		return dimension.substr(dimension.length-2,2);
	},
	
	timebarRender: function(cJSON) {
		var tJSON = _.extend({},cJSON);
		var template_timebar = this.template_timebar;
		var template_timebar_block = this.template_timebar_block;
		var o = this.options;
		
		tJSON.width_section1 = (this.dimensNum(cJSON.width_section1) - 1) + cJSON.width_units;
		tJSON.timebar_section1_id = o.timebar_id_core + "_" + o.view_instance + "_" + "section1"; 
		tJSON.timebar_section2_id = o.timebar_id_core + "_" + o.view_instance + "_" + "section2";		

		// tJSON.timebar_section1_id = o.timebar_id_core + "_" + o.view_instance + "_" + this.padZeroes(subview_seq);; 
		tJSON.html_timebar = "";
		var html_timebar_blocks = "";
		var startTime = this.dateMillsec(tJSON.ipg_start_time);
		var endTime = this.dateMillsec(tJSON.ipg_end_time);
		var workTime = startTime;
		
		if (endTime > startTime) {
			var subview_seq = 1;
			var border_compensate = 0;
			while (workTime < endTime) {
				var border_compensate = (subview_seq > 1) ? 1 : 1;
				tJSON.timeblock_view_id = o.timebar_id_core + "_" + o.view_instance + "_" + this.padZeroes(subview_seq,3);
				tJSON.timebar_block_width = (Math.round(0.5 * tJSON.width_per_hour) - border_compensate) + tJSON.width_units;
				tJSON.display_time = this.localDisplayTime(new Date(workTime));
				html_timebar_blocks += template_timebar_block(tJSON);
				workTime += 1800000;
				subview_seq++;
			} //**END while 	
		}
		
		tJSON.html_timebar_blocks = html_timebar_blocks;
		var html_timebar_view = template_timebar(tJSON);
		
		return html_timebar_view;
	},
	
    render: function() {
		//** assign DOM selectors and IDs according to custom convention
		
		var cJSON = {};
		var htmlText = "";
		var template = this.template;
		var template_inner = this.template_inner;

		var timebarRender = this.timebarRender;
		var o = this.options;
		var container_selector = o.container_selector;
		var view_id_pref = o.view_id_core + "_" + o.view_instance + "_";
		var view_id_attr = o.view_id_attr;
		var view_height_num = this.dimensNum(o.view_height);
		cJSON.width_per_hour = this.dimensNum(o.pixels_per_hour);
		cJSON.width_units = this.dimensPx(o.pixels_per_hour);
		cJSON.image_dir = o.image_dir;
		cJSON.width_section1 = Math.round(cJSON.width_per_hour/2);
		cJSON.width_section2 = o.width_in_hours * cJSON.width_per_hour;
		cJSON.view_width = (cJSON.width_section1 + cJSON.width_section2) + cJSON.width_units;
		cJSON.width_section1 += cJSON.width_units;
		cJSON.width_section2 += cJSON.width_units;
		cJSON.view_height = o.view_height;
		cJSON.timebar_height = o.timebar_height;
		cJSON.template_inner = o.template_inner;
		
		var ii = 0;
		
		this.collection.each(function(model) {
			var programList = model.get('programList');
			cJSON.view_id = view_id_pref + model.get(view_id_attr);
			cJSON.in_room_id = model.get('in_room_id');
			cJSON.icon = model.get('icon');
			cJSON.callLetters = model.get('callLetters');
			cJSON.ipg_start_time = model.get('ipg_start_time');			
			cJSON.ipg_end_time = model.get('ipg_end_time');
			if(cJSON.callLetters.indexOf("HD") == (cJSON.callLetters.length - 2)) {
				cJSON.callLetters = cJSON.callLetters.substr(0,cJSON.callLetters.length-2);
			}
			
			/* test mode: compensate for zero in_room channel numbers */
			if(o.test_mode) {
				if(cJSON.in_room_id == "0") {
					cJSON.in_room_id = Math.floor(Math.random() * 50) + 3;
				}
			}
			

			
			//** Intermedate development: timebar
			if(!ii) {
				var html_timebar = timebarRender(cJSON);
				$(container_selector).append(html_timebar);
			}
			
			var html_subview = "";			
			cJSON.subview_seq = 1;
			programList.each(function(pgm) {
				var subview = new ProgramView({model:pgm,channel_parms:cJSON});
				html_subview += subview.render();
				cJSON.subview_seq++;
			}); //**END each programList
			
			 cJSON.html_subview = html_subview;
			
			
			// cJSON.a = "M" + ii++;
			// cJSON.b = "L" + ii;
			// cJSON.html_inner = template_inner(pJSON);
			
			//** add a channel with its programs
			htmlText += template(cJSON);
			ii++;
	        
		}); //**END each collection

		$(container_selector).append(htmlText);
		return this;
    },


});
//** END ChannelView
$(document).ready(function(){
	
	// var programModel = new ProgramModel();
	// var programList = new ProgramList({model:programModel});
	
	// var channelModel = new ChannelModel();
	var channelList = new ChannelList();
	
	var view = new ChannelView({collection:channelList, 
		container_selector:'#channel-list', 
		test_mode:1});
	
	view.loadDocument();
	
});
// }); //**END JQuery wrapper
</script>
</head>
<body>
<button id="load-input">Load Channels</button>


<div id="channel-list" class="container rounded skin-whitish">
	
</div>

</body>
</html>