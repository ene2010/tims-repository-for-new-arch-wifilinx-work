<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <title>Mobile Test 3</title>
    <link rel="stylesheet" href="/lib/org/jquery.mobile/jquery.mobile-1.2.0.css" />
    <script type="text/javascript" src="/lib/org/jquery/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="/lib/org/jquery.mobile/jquery.mobile-1.2.0.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!--Fix for header close button text being unconditionally suppressed -->
    
    
    
</head>

<body>

    
    <div data-role="page" id="page_dialog" data-title="pop-up" data-close-btn-text="Close">
    	<div data-role="header">
            <a href="#page1" data-icon="delete">Close</a>  
            <h1>Test 03 Pop-up</h1>
    	</div>
        <div data-role="content">
        	<p>Dialog from external page.</p>
        	<p>This is test02_dialog.php</p>
        </div>
        <div data-role="footer" data-position="fixed">
        	<h4>Pop-up footer text</h4>
    	</div>
    </div>    


    <script type="text/javascript">
		var objSpan = $("#page_dialog .ui-btn-icon-notext");
    	objSpan.removeClass(".ui-btn-icon-notext"); 
	</script>
    
</body>

</html>
    
