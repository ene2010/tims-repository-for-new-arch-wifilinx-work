<?php
/*
* @script: global config file for portal page framework
*/

// System stuff
$system_hostname    = php_uname('n');
$php_version        = phpversion();
$corpID             = "Roomlinx";
$Pcode              = "linx";
$PropID             = "1";
$vendor             = "rmlx";
$template_set       = "linx2";
// SET FILE PATHS
ini_set('include_path','/var/www/lib/Zend:/var/www/lib/jpgraph/src:/var/www/lib/Smarty');
define('SMARTY_DIR', '/var/www/lib/Smarty/');
include(SMARTY_DIR . 'Smarty.class.php');

//session items
ini_set("session.save_path", "/var/www/logs/sesstmp"); 
session_start();
$ses_id = session_id();

// Define Mysql connection information
$sql_host = "localhost";
$sql_user = "root";
$sql_pass = "ro0ml1nxSS";
$sql_db   = "sessiondb";
$rad_db   = "rmlx_radiusdb";
$DBH;


require_once(dirname(__FILE__).'/wfl_helpers.php');
require_once(dirname(__FILE__).'/wfl_db.php');
require_once(dirname(__FILE__).'/wfl_packages.php');
// Connect to the DB

ODbConnect();
   $Session_connection = mysql_pconnect($sql_host, $sql_user, $sql_pass);
    mysql_select_db($rad_db, $Session_connection);


//define the remote xml urls to parse
$apiserver["master"]    = "http://172.29.2.30/api2";
$apiserver["slave"]     = "http://172.29.2.31/api2";
$apiserver["masterlb"]  = "http://172.29.2.29/api2";
$apiserver["session"]  = "https://$system_hostname/ws_wifi";

$authapi["master"]      = "https://$system_hostname/ws_wifi";

$active_authapiurl  = $authapi["master"];
$activeapi_url      = $apiserver["master"];
$session_api_url     = $apiserver["session"];

//portal page settings
$sess_portalurl              = "https://$system_hostname/wfl-portal/index.php";
$property_portalurl          = "https://$Pcode.roomlinx.com/wfl-portal/index.php";
$property_portaldash         = "https://$Pcode.roomlinx.com/wfl-portal/dashboard.php";
$mikrotik_gateway            = "https://wflgw.roomlinx.com";
$mikrotik_logout             = "$mikrotik_gateway/logout";
$mikrotik_login              = "$mikrotik_gateway/login";
$mikrotik_status             = "$mikrotik_gateway/status";
$mikrotik_user_stats         = "$mikrotik_gateway/userstats.html"; 
//web layout
$layoutType             = initLayoutType();

//smarty core settings
$portal_base 	    = "/var/www/wifilinx/wfl-portal/portal";
$property_base      = "$portal_base/$template_set";
$portal_pagebase    = "$property_base/";



// Smarty global

$Psmarty = new Smarty();
$Psmarty->template_dir  = ''.$portal_pagebase.'';
$Psmarty->compile_dir   = ''.$portal_pagebase.'/templates_c';
$Psmarty->cache_dir     = ''.$portal_pagebase.'/cache';
$Psmarty->config_dir    = ''.$portal_pagebase.'/configs';
$Psmarty->plugins_dir   = '/var/www/lib/Smarty/plugins';
$Psmarty->caching       = false;
$Psmarty->force_compile = true;
$Psmarty->debugging     = false;

//GLOBAL SMARTY ASSIGNMENTS
$Psmarty->assign("PhpVer", $php_version);
$Psmarty->assign("SysHostname", $system_hostname);
$Psmarty->assign("RmlxCustomer", $corpID);
$Psmarty->assign("PropertyCode", $Pcode);
$Psmarty->assign("PropertyID", $PropID);
$Psmarty->assign("PropertyPortalURL", $property_portalurl);
$Psmarty->assign("PropertyPortalDashboard", $property_portaldash);
$Psmarty->assign("SessionServerAPI", $session_api_url);
$Psmarty->assign("MikroTikGW", $mikrotik_gateway);
$Psmarty->assign("MikroTikLogout", $mikrotik_logout);
$Psmarty->assign("MikroTikLogin", $mikrotik_login);
$Psmarty->assign("MikroTikStatus", $mikrotik_status);
$Psmarty->assign("BasePath", "/wfl-portal");  
$Psmarty->assign("fullBasePath", "$portal_pagebase/");
$Psmarty->assign("mobileBasePath", "/wfl-portal/portal/$template_set/mobile/");
$Psmarty->assign("mobileImagePath", "/wfl-portal/portal/$template_set/mobile/images");
$Psmarty->assign("mobileCssPath", "/wfl-portal/portal/$template_set/mobile/css");
$Psmarty->assign("mobileJsPath", "/wfl-portal/portal/$template_set/mobile/js");
$Psmarty->assign("imagePath", '/wfl-portal/portal/'.$template_set.'/images');
$Psmarty->assign("cssPath", '/wfl-portal/portal/'.$template_set.'/css');
$Psmarty->assign("jsPath", '/wfl-portal/portal/'.$template_set.'/js');

?>
