<?php
require_once(dirname(__FILE__) . "/include/config.global.tene.php");

$packages = array();
$packages[]  = array("id"=>"1","description"=>"Standard Internet","price"=>"$9.95");
$packages[]  = array("id"=>"2","description"=>"Premium Internet","price"=>"$14.95");
$packages[]  = array("id"=>"3","description"=>"Premium Plus","price"=>"$19.95");
	
$Psmarty->assign("packages",$packages);
$Psmarty->display("testsmarty.tpl");
	
?>