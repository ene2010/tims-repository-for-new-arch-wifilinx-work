<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name=apple-mobile-web-app-capable content=yes>
	<meta name=apple-mobile-web-app-status-bar-style content=black>
    <title>Roomlinx Test</title>
    <link rel="stylesheet" href="lib/org/jquery.mobile/jquery.mobile-1.2.0.css" />
    <script type="text/javascript" src="lib/org/jquery/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="lib/org/jquery.mobile/jquery.mobile-1.2.0.js"></script>
    
    <!-- Custom style fixes for JQM bugs -->
    <style>
		.cust-ui-li-aside {
			width:15%;
			margin: 0.7em 3em;	
		}
	
		.ui-body-c {
			background: #111;
			color:#fff;
		}				
		
		#top_header_content {
			display-block;
		}
		
		#h1-text {
			float: left;
			color:#fff;
			font-family:Arial;
			font-size:1.5em;
			font-weight:normal;
			line-height:3em;
			text-transform:uppercase;
		}
		
		#h1-img {
			margin:10px 20px 0 35px;
			float:left;
			border:none;
		}
		
		/** Diagnostics **/
		.ix-iphone-port, .ix-iphone-land,
		.ix-ipad-port, .ix-ipad-land,
		.ix-desktop-port, .ix-desktop-land,
		.ix-desktop-narrow {
			display:none;
			float: left;
			color:#fff;
			font-family:Arial;
			font-size:1.5em;
			font-weight:normal;
			line-height:3em;
			text-transform:uppercase;
		}

		/** wide-window desktops, ipad landscape **/
		@media screen and (min-width:1260px){
			.ui-header .ui-title {
			    margin-right: 20%;
			    margin-left: 20%;
				overflow:hidden;
				white-space:nowrap;
			}
			
			.ix-desktop-land {
				display:block;
			}	
			
		} /** END media **/
		
		/** wide-window desktops, ipad landscape **/
		/*---------------*/
		@media screen and (min-width:1024px) and (max-width:1259px) {
			.ui-header .ui-title {
			    margin-right: 10%;
			    margin-left: 10%;
				overflow:hidden;
				white-space:nowrap;
			}
			
			.ix-desktop-narrow {
				display:block;
			}	
			
		} /** END media **/
		
		/*-------------------*/
	
		/** narrow-window desktops, ipad portrait, iphone **/
		@media screen and (max-width:1023px) {
			.ui-header .ui-title {
			    margin-right: 0%;
			    margin-left: 0%;
				overflow:hidden;
				white-space:ellipsis;
			}
			
			#top_header h1 {
				overflow: ellipsis;
				white-space:nowrap;
				height: 6em;
			}
			
			#top_header {
				overflow: ellipsis;
				white-space:nowrap;
			}
			
			.ix-desktop-port {
				display:block;
			}	
		}	/** END media **/
		
		/** iPhone portrait, retina and non-retina **/
		@media screen and (max-width:640px) and (orientation:portrait) {
			
			h1 {
				overflow: hidden;
				white-space:nowrap;
			}
			
			#top_header {
				overflow: hidden;
				white-space:nowrap;
			}
			
			#h1-text {
				margin-top:0.75em;
				font-size:0.60em;
				font-weight:normal;
				line-height:1.5em;
				overflow:hidden;
				white-space:nowrap;
			}
		
			
			#h1-img {
				width:20%;
				margin:0.25em 10px 0 20px;
				float:left;
				border:none;
			}
			
			.ix-iphone-port {
				display:block;
			}			
			
		} /** END media **/
		
		/** iPhone portrait, retina and non-retina **/
		@media screen and (max-width:640px) and (orientation:landscape) {
			
			h1 {
				overflow: hidden;
				white-space:nowrap;
			}
			
			#top_header {
				overflow: hidden;
				white-space:nowrap;
			}
			
			#h1-text {
				margin-top:0.75em;
				font-size:0.75em;
				font-weight:normal;
				line-height:1.5em;
				overflow:hidden;
				white-space:nowrap;
			}
		
			
			#h1-img {
				width:20%;
				margin:0.25em 20px 0 20px;
				float:left;
				border:none;
			}
			
			.ix-iphone-land {
				display:block;
			}			
			
		} /** END media **/
			
		
		/** header horizontal gradient, but not iphone portrait **/ 
		@media screen and (min-width:768px) {
			.ui-bar-b {
				border: 1px solid #4d4d4d;
				/* Firefox */
				background: -moz-linear-gradient(top,#222222 50%,#1b1b1b 100%);
			
				/* Safari and Chrome */
				background: -webkit-linear-gradient(top,#222222 50%,#1b1b1b 100%);
			
				/* IE */
				*filter: progid:DXImageTransform.Microsoft.gradient(gradientType=1,startColorstr='#000000', endColorstr='#000000');
			}

			/* .ui-bar-a{ */
			#top_header {
				color: #000;
				/* Firefox */
				background: -moz-linear-gradient(top,#000000 25%,#111111 37%,#232323 50%,#151515 75%,#000000 100%);
			
				/* Safari and Chrome */
				background: -webkit-linear-gradient(top,#000000 25%,#111111 37%,#232323 50%,#151515 75%,#000000 100%);
			
				/* IE */
				*filter: progid:DXImageTransform.Microsoft.gradient(gradientType=1,startColorstr='#000000', endColorstr='#000000');
				text-align: center;
			}			
			
		} /** END media **/
		
		.ui-bar-a {
			background: #000;
			color:#ffffff;
			border-bottom: 1px solid #000000;
			
		}
		
		.ui-btn-text * {
			color:#FFFFFF;
			text-shadow:0 1px 0 #000;
		}
		
	</style>
	
	<!-- ><link rel="stylesheet" href="jquery.mobile.1.0.css" /> -->
	
</head>

<body>

	<!-- The slide-up to hide the URL control doesn't work in iOS 6 Chrome -->

	<div data-role="page" id="page1">
    	<div id="top_header" data-role="header" data-theme="a">
			
			<!---- -->
			<h1>
				<div id="top_header_content">
					<img id="h1-img" border="0" title="Hyatt" alt="Hyatt" src="image/logo-hyattregency5.png">
		        	<div id="h1-text">welcome to hyatt regency houston</div>
				</div>
				<!--
				<div class="ix-iphone-port">iPhone Portrait</div>
				<div class="ix-iphone-land">iPhone Landscape</div>
				<div class="ix-ipad-port">iPad Portrait</div>
				<div class="ix-ipad-land">iPad Landscape</div>
				<div class="ix-desktop-port">Desktop Portrait</div>
				<div class="ix-desktop-land">Desktop Landscape</div>
				<div class="ix-desktop-narrow">Desktop Narrow</div>
				---->
				
			</h1>
			<!-- >-------->
				
				<!-- 
				<div id="top_header_content">
					<img id="h1-img" border="0" title="Hyatt" alt="Hyatt" src="image/logo-hyattregency5.png">
		        	<div id="h1-text">welcome to hyatt regency houston</div>
				</div>
				---->
        </div>
        
        <!-- Content -->
        <div data-role="content">
            
             <!-- COMMENT OUT             
             <ul data-role="listview" data-inset="true" data-theme="a" data-divider-theme="d">
             	<li data-role="list-divider">Split Button Inset List</li>          
                <li>
                	<a href="#page2" data-transition="slide">Link to go to the second page</a>
                    <a href="#page2"></a>
                </li>
                <li data-transition="slide">
                	<a href="http://www.mobilexweb.com" data-rel="external" 
                    	data-transition="slide">Link to an absolute external page</a>
                    <a href="http://www.mobilexweb.com"></a>
                </li>
                <li>
                	<a href="dialog.php" data-rel="dialog">Link to an external dialog page:</a>
                    <a href="dialog.php" data-rel="dialog"></a>
                </li>
                <li>
                	<a href="#page_dialog" data-rel="dialog">Link to an internal dialog page:</a>
                    <a href="#page_dialog" data-rel="dialog"></a>
                </li>
        	</ul>
           
             <ul data-role="listview" data-inset="true" data-theme="a">
             	<li data-role="list-divider">Data Inset List</li>          
                <li>
                	<a href="#page2" data-transition="slide">Link to go to the second page</a>
                </li>
                <li>
                	<a href="http://www.mobilexweb.com" data-transition="slide" data-rel="external">
                    Link to an absolute external page</a>
                </li>
                <li>
                	<a href="dialog.php" data-rel="dialog">Link to an external dialog page:</a>
                </li>
                <li>
                	<a href="#page_dialog" data-rel="dialog">Link to an internal dialog page:</a>
                </li>
        	</ul>
            
             <ul data-role="listview" data-inset="true" data-theme="a">
             	<li data-role="list-divider">List with Asides</li>          
                <li><a href="#page2" data-transition="slide">Second page</a>
                    <span class="ui-li-aside cust-ui-li-aside">$1.00</span>
                </li>
                <li><a href="dialog.php" data-rel="dialog">Link to an external dialog page:</a>
                	<span class="ui-li-aside cust-ui-li-aside">$12.00</span>
                </li>
                <li>
                	<a href="#page_dialog" data-rel="dialog">Link to an internal dialog page:</a>
                    <span class="ui-li-aside cust-ui-li-aside">$100.00</span>
                </li>
        	</ul>
			COMMENT OUT -->

		<!--div class="content-primary" -->
		<ul data-role="listview" data-inset="true" data-theme="a" data-corners="false" data-shadow="false">
        	<li data-role="list-divider" data-corners="false" >List with Icons from JQM Demo</li>
			<li><a href="index.html"><img src="/lib/org/jquery.mobile/demos/docs/lists/images/gf.png" 
            	alt="France" class="ui-li-icon">France <span class="ui-li-count">4</span></a></li>
			<li><a href="index.html"><img src="/lib/org/jquery.mobile/demos/docs/lists/images/de.png" 
            	alt="Germany" class="ui-li-icon">Germany <span class="ui-li-count">4</span></a></li>
			<li><a href="index.html"><img src="/lib/org/jquery.mobile/demos/docs/lists/images/gb.png" 
            	alt="Great Britain" class="ui-li-icon">Great Britain <span class="ui-li-count">0</span></a></li>
			<li><a href="index.html"><img src="/lib/org/jquery.mobile/demos/docs/lists/images/fi.png" 
            	alt="Finland" class="ui-li-icon">Finland <span class="ui-li-count">12</span></a></li>
			<li><a href="index.html"><img src="/lib/org/jquery.mobile/demos/docs/lists/images/sj.png" 
            	alt="Norway" class="ui-li-icon">Norway <span class="ui-li-count">328</span></a></li>
			<li><a href="index.html"><img src="/lib/org/jquery.mobile/demos/docs/lists/images/us.png" 
            	alt="United States" class="ui-li-icon">United States <span class="ui-li-count">62</span></a></li>
		</ul>

		<!--/div --><!--/content-primary -->		

            
            <!-- COMMENT OUT: Use of JQM columns is ineffective at laying text and buttons side-by-side
            <section class="ui-grid-a">
                <div class="ui-block-a">Link to go to the</div>
                <a class="ui-block-b" href="#page2" data-transition="slide">second page</a>
                <div class="ui-block-a">Link to an </div>
                <a class="ui-block-b" href="http://www.mobilexweb.com" data-rel="external">absolute external page</a>
                <div class="ui-block-a">Link to an external dialog page:</div> 
                <a class="ui-block-b" href="dialog.php" data-role="button" data-rel="dialog" data-inline="true" data-mini="true" data-prefetch>Open dialog</a>
                <div class="ui-block-a">Link to an internal dialog page:</div> 
                <a class="ui-block-b" href="#page_dialog" data-role="button" data-rel="dialog" data-inline="true"  data-mini="true">Open dialog</a>
        	</section>
            END-COMMENT -->
            
            
            
        </div>
        <!-- End Content -->
        
        <div data-role="footer" data-position="fixed">
        	<h4>Here's some footer content</h4>
         </div>
    </div>
    
	<div data-role="page" id="page2" data-title="Data-title of 2nd Page" data-add-back-btn="true" data-back-btn-theme="d">
    	<div data-role="header">
        	<h1>Test 03 Page 2</h1>
        </div>
        <div data-role="content">
        	<p>This is the content of the second page</p>
            <p>Here's one method of going back: <br /><a href="#page1" data-transition="slide" data-rel="back">first page</a>.</p>
        </div>
        <div data-role="footer" data-position="fixed">
        	<!--h4>Here's some 2nd page footer content</h4-->
            <div data-role="navbar">
            	<ul>
                	<li><a href="#page1">New</a></li>
                	<li><a href="#page1">Search</a></li>
                	<li><a href="#page1">Filter</a></li>
                	<li><a href="#page1">Delete</a></li>
                </ul>
         	</div>
    	</div>
    </div>
    
    <!-- This dialog was unsatisfactory, since couldn't add "Close" text to default left-side close button -->
    <!-- The button's span had data-iconpos="notext"; couldn't get rid of it with markup alone. -->
    <!-- Trying for a "popup" rather than dialog (see next code block) -->
    <!-- COMMENT-OUT BEGIN
    <div data-role="page" data-close-btn-text="Close" data-iconpos="right" id="page_dialog" data-title="pop-up">
    	<div data-role="header">
            <a href="#page1" data-icon="delete">Close</a>  
            <h1>Test 02 Pop-up</h1>
    	</div>
        <div data-role="content">
        	<p>This is the content of the pop-up page</p>
        </div>
        <div data-role="footer">
        	<h4>Pop-up footer text</h4>
    	</div>
    </div>
    COMMENT-OUT END -->


    <div data-role="page" data-close-btn-text="Close" id="page_dialog" data-title="pop-up">
    	<div data-role="header">
            <a href="#page1" data-icon="delete">Close</a>  
            <h1>Test 03 Pop-up</h1>
    	</div>
        <div data-role="content">
        	<p>Dialog from internal page.</p>
        </div>
        <div data-role="footer" data-position="fixed">
        	<h4>Pop-up footer text</h4>
    	</div>
    </div>
    
    
</body>

</html>
    
