// Structr.js 0.0.1 framework
// Dependency: Underscore.js
// Dependency: jQuery


//** function execution will create the variables below in open namespace
(function(){
	
	//** "exteriorize" Structr object by anchoring it to window object
	var wdw = this; 
	wdw.Structr = {};
	
	//** Model object --------------------------------------------
	Structr.Model = {
		
		attributes : {},
		
		get: function(attribute) {
			return (this.attributes[attribute]) ? this.attributes[attribute] : null;
		},
		
		set: function(attribute, value, options) {
			//** only allow setting of existing attributes defined through initial extend()
			if(!this.attribute) return false;
			this.attribute = value;
			return true;
		},
		
	};

	//** Collection object --------------------------------------------
	Structr.Collection = {
			url:"",
			dataType:"xml",
			freshFetch:true,
			
			errorPrefix:"Structr.js Error: ",
			that: this,

			models: [],
		
		test:"asdfsdf",
	
		//** extend() capability
		extender: function(extension) {
			extension = extension || {};
			return _.extend(this,extension);
		},
		
		//** asynch fetch
		fetch: function(){
			var szRequestString = "";
			var szRandom = "";
			var objRequest = null;
			
			//** error if invalid options
			if(!url || url.length() < 5) {
				throw new Error(errorPrefix + "invalid url specified for Collection.");
			}
			
			//** prepare request url string
			szSep = (url.indexOf("?") != -1) ? "&" : "?";
			szRandom = freshFetch ? (szSep + "random=" + Math.random()) : ""; 
			szRequest = url + szRandom;
			
			//** make xhttp request
			$.ajax({
				type: "GET",
				dataType: dataType,
				url: url,
				success: function(data) {
					that.metaParser(data);
				}
			}); //**END $.ajax
		}, //**END fetch()

		//** initial parse response data to obtain array of nodes in the response's format: XML or JSON
		metaParser: function(data) {
			var nodes = [];
			if
		},
		
		//** parse response data. Will have overridden using extend() to get data into models
		parse: function(data) {
			
		}
		
		
	}; //** END Collection
	
	//** General functions --------------------------------------------
	
	//** extend() borrowed from Backbone.js
    Structr.extend = function(protoProps, staticProps) {
      var parent = this;
      var child;

      // The constructor function for the new subclass is either defined by you
      // (the "constructor" property in your `extend` definition), or defaulted
      // by us to simply call the parent's constructor.
      if (protoProps && _.has(protoProps, 'constructor')) {
        child = protoProps.constructor;
      } else {
        child = function(){ return parent.apply(this, arguments); };
      }

      // Add static properties to the constructor function, if supplied.
      _.extend(child, parent, staticProps);

      // Set the prototype chain to inherit from `parent`, without calling
      // `parent`'s constructor function.
      var Surrogate = function(){ this.constructor = child; };
      Surrogate.prototype = parent.prototype;
      child.prototype = new Surrogate;

      // Add prototype properties (instance properties) to the subclass,
      // if supplied.
      if (protoProps) _.extend(child.prototype, protoProps);

      // Set a convenience property in case the parent's prototype is needed
      // later.
      child.__super__ = parent.prototype;

      return child;
    };
	
	Model.extend = Collection.extend = View.extend = Structr.extend;
	
	

}).call(this);